import csv

import os
from flask import Flask, request, flash, redirect, url_for, render_template
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = {'csv'}

app = Flask(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def parse_csv(filename):
    rv = list()
    with open(os.path.join(UPLOAD_FOLDER, secure_filename(filename))) as f:
        csvreader = csv.reader(f)
        iterable = list(csvreader)
        for i, row in enumerate(iterable[1:]):
            rv.append(dict(
                team_name=row[0],
                team_table_number=row[1],
                short_description='<br>'.join(row[7].split('\n')),
                team_lead_name=row[2],
                team_lead_contact_details="{}<br>{} ".format(row[3], row[4]),
                open_to_additional_people_joining=row[5],
                challenges=row[6],
            ))
        return rv


@app.route("/teams")
def get_teams():
    filename = request.args.get('filename')
    teams = parse_csv(filename)
    return render_template("teams.html", teams=teams)


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            return redirect(url_for('get_teams',
                                    filename=filename))
    return render_template("index.html")


app.run(debug=True)
