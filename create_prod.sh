#!/usr/bin/env bash
set -e

IP=IPV4
if [ ! -f "prod_config.sh" ]; then
    echo "Copy from prod_config.sh.sample with correct values"
    exit 1;
fi

. prod_config.sh

echo "LOCAL: Connecting to root@${IPV4} - 1. root provision #1"
echo "======================================="
echo "PROJECT_USER=${PROJECT_USER}; PROJECT_DIR=${PROJECT_DIR}; $(cat provision/prod_1_root.sh)" | ssh root@${IPV4} /bin/bash

echo "LOCAL: Copy SSH key to root@${IPV4}:/home/${PROJECT_USER}/.ssh"
echo "======================================="
scp sensitive.py root@${IPV4}:/home/${PROJECT_USER}/
scp ~/.ssh/${SKEY} root@${IPV4}:/home/${PROJECT_USER}/.ssh/
scp ~/.ssh/${SKEY}.pub root@${IPV4}:/home/${PROJECT_USER}/.ssh/authorized_keys

echo "LOCAL: Connecting to root@${IPV4} - 2. root provision"
echo "======================================="
echo "PROJECT_USER=${PROJECT_USER}; $(cat provision/prod_2_root.sh)" | ssh root@${IPV4} /bin/bash

echo "LOCAL: Connecting to ${PROJECT_USER}@${IPV4} - 3. user provision"
echo "======================================="
echo "SKEY=${SKEY}; PROJECT_DIR=${PROJECT_DIR}; PROJECT=${PROJECT}; PROJECT_USER=${PROJECT_USER}; PROJECT_DOMAIN=${PROJECT_DOMAIN}; $(cat provision/prod_3_user.sh)" | ssh ${PROJECT_USER}@${IPV4} /bin/bash

echo "LOCAL: Connecting to root@${IPV4} - 4. root provision"
echo "======================================="
echo "PROJECT_DIR=${PROJECT_DIR}; PROJECT=${PROJECT}; PROJECT_DOMAIN=${PROJECT_DOMAIN}; IPV6=${IPV6_GATEWAY}; IPV6=${IPV6_GATEWAY}; IPV6_NS=${IPV6_NS}; $(cat provision/prod_4_root.sh)" | ssh root@${IPV4} /bin/bash