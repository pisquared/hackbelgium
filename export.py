import sys
import csv
from jinja2 import Environment, PackageLoader, select_autoescape
env = Environment(
    loader=PackageLoader('export', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)

if len(sys.argv) == 2:
    fname = sys.argv[1]
else:
    fname = sys.argv[0]

template = env.get_template('teams.html')

with open(fname) as f:
    csvreader = csv.reader(f)
    iterable = list(csvreader)
    for i, row in enumerate(iterable[1:]):
        ctx = dict(
            team_name=row[0],
            team_table_number=row[1],
            short_description='<br>'.join(row[7].split('\n')),
            team_lead_name=row[2],
            team_lead_contact_details="{}<br>{} ".format(row[3], row[4]),
            open_to_additional_people_joining=row[5],
            challenges=row[6],
        )
        w = template.render(**ctx)

        with open("exports/{}.html".format(i), 'w+') as wf:
            wf.write(w)

