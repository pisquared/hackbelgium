#!/usr/bin/env bash
set -e

echo "REMOTE: Entering ${PROJECT_DIR}"
echo "======================================="
cd ${PROJECT_DIR}

echo "REMOTE: Installing gunicorn service"
echo "======================================="
cp provision/systemd.service "/etc/systemd/system/${PROJECT}.service"
systemctl daemon-reload
systemctl enable ${PROJECT}
systemctl start ${PROJECT}

if [ ! -f "/etc/letsencrypt/live/${PROJECT_DOMAIN}/fullchain.pem" ]; then
    echo "REMOTE: Generating letsencrypt certificate"
    echo "======================================="
    [ ! -f /tmp/certbot-auto ] && wget -O /tmp/certbot-auto https://dl.eff.org/certbot-auto
    chmod +x /tmp/certbot-auto
    /tmp/certbot-auto --authenticator standalone --installer nginx --pre-hook "service nginx stop" --post-hook "service nginx start" --redirect --agree-tos --no-eff-email --email admin@app.com -d ${PROJECT_DOMAIN} -d www.${PROJECT_DOMAIN} --no-bootstrap
fi

echo "REMOTE: Copying over nginx config"
echo "======================================="
if [ -f /etc/nginx/sites-enabled/default ]; then
  rm /etc/nginx/sites-enabled/default
fi
cp provision/nginx.conf /etc/nginx/sites-available/${PROJECT}.conf
[ ! -f /etc/nginx/sites-enabled/${PROJECT}.conf ] && \
  ln -s /etc/nginx/sites-available/${PROJECT}.conf /etc/nginx/sites-enabled/${PROJECT}.conf && \
  systemctl restart nginx

echo "REMOTE: Enable IPv6"
echo "======================================="
cat > /etc/network/interfaces << EOF
iface eth0 inet6 static
        address ${IPV6}
        netmask 64
        gateway ${IPV6_GATEWAY}
        autoconf 0
        dns-nameservers ${IPV6_NS}
EOF