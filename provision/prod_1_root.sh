#!/usr/bin/env bash
echo "REMOTE: Installing nginx"
echo "======================================="
apt-get update
apt-get install -y nginx python-virtualenv

echo "REMOTE: Creating ${PROJECT_USER} user"
echo "======================================="
useradd -m ${PROJECT_USER}
mkdir -p /home/${PROJECT_USER}/.ssh
mkdir -p ${PROJECT_DIR}
chown ${PROJECT_USER} ${PROJECT_DIR}