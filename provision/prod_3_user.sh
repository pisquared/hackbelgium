#!/usr/bin/env bash
echo "REMOTE: Cloning repo"
echo "======================================="
eval `ssh-agent`
ssh-add ~/.ssh/${SKEY}
ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
if [ "$(ls -A ${PROJECT_DIR})" ]; then
    # directory not empty
    cd ${PROJECT_DIR}
    git pull origin master
else
    git clone git@gitlab.com:pisquared/${PROJECT}.git ${PROJECT_DIR}
fi

cd ${PROJECT_DIR}

echo "REMOTE: Creating virtualenv"
echo "======================================="
virtualenv -p python3 venv
source venv/bin/activate

echo "REMOTE: Installing python packages"
echo "======================================="
pip install -r requirements.txt

sed -e "s|User=.*|User=${PROJECT_USER}|g" provision/systemd.service.sample > provision/systemd.service
sed -i -e "s|WorkingDirectory=.*|WorkingDirectory=${PROJECT_DIR}|g" provision/systemd.service
sed -e "s|example.com|${PROJECT_DOMAIN}|g" provision/nginx.conf.sample > provision/nginx.conf

exit 0
