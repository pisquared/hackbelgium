#!/usr/bin/env bash
echo "REMOTE: Change ownership of private key"
echo "======================================="
chown -R ${PROJECT_USER}:${PROJECT_USER} /home/${PROJECT_USER}/.ssh
chmod 700 /home/${PROJECT_USER}/.ssh
chmod 600 /home/${PROJECT_USER}/.ssh/*
